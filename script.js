/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const owner = 'Rangaraj99';
const repoName = 'demo';
const apiUrl = `https://api.github.com/repos/${owner}/${repoName}/issues`;

// Color of the button and state based on the title value
function checkTitle() {
  const title = document.getElementById('issue-title').value;
  const newIssueBtn = document.getElementById('newIssueBtn');
  if (title.trim() !== '') {
    newIssueBtn.style.backgroundColor = '#238636';
    newIssueBtn.removeAttribute('disabled');
  } else {
    newIssueBtn.style.backgroundColor = '#8b9f64';
    newIssueBtn.setAttribute('disabled', 'true');
  }
}

//show popup message after a crud operation
function showPopup(message) {
  const popup = document.getElementById('popup');
  popup.innerText = message;
  popup.style.display = 'block';

  setTimeout(() => {
    popup.style.display = 'none';
  }, 4000);
}

//function to create a new issue
async function createIssue() {
  try {
    const title = document.getElementById('issue-title').value;
    const body = document.getElementById('issue-description').value;
    const data = { title, body };
    await CRUDoperation(apiUrl, 'POST', data);
    showPopup('New Issue Created Successfully!');
    console.log('New issue created.');
    document.getElementById('issue-title').value = '';
    document.getElementById('issue-description').value = '';
    checkTitle();
  } catch (error) {
    console.error(error.message);
  }
}

//function to do the api calls with different methods
async function CRUDoperation(apiUrl, method, data) {
  const accessToken = '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';
  const requestOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  };
  const response = await fetch(apiUrl, requestOptions);
  return await response.json();
}

//fucntion to fetch issues with 'get' method
async function getIssues(apiUrl, issueType) {
  const issues_data = await CRUDoperation(apiUrl, 'GET');
  const issues = document.querySelector('.content');
  const createIssueContainer = document.querySelector('.create-issue');
  issues.classList.remove('display-none');
  createIssueContainer.classList.add('display-none');
  issues.innerHTML = '';
  if (issues_data.length == 0) {
    const h4 = document.createElement('h4');
    h4.textContent = 'No Issues to Display!';
    issues.appendChild(h4);
  }
  issues_data.forEach((issue) => {
    const div = document.createElement('div');
    div.className = 'issue_content';
    div.setAttribute('id', `issue_id_${issue.number}`);

    const h3 = document.createElement('h3');
    h3.textContent = `Issue number : ${issue.number}`;
    div.appendChild(h3);

    const title_h5 = document.createElement('h5');
    title_h5.textContent = `Issue Title :`;
    div.appendChild(title_h5);

    const title_text = document.createElement('input');
    title_text.value = issue.title;
    title_text.readOnly = true;
    div.appendChild(title_text);

    const description_h5 = document.createElement('h5');
    description_h5.textContent = `Issue Description :`;
    div.appendChild(description_h5);

    const description_text = document.createElement('textarea');
    description_text.value = issue.body;
    description_text.readOnly = true;
    div.appendChild(description_text);

    if (issueType == 'close') {
      const reopen_button = document.createElement('button');
      reopen_button.setAttribute('class', 'update-buttons');
      reopen_button.innerText = 'Reopen Issue';
      reopen_button.setAttribute('onclick', `reopenProcess(${issue.number})`);
      div.appendChild(reopen_button);
    } else {
      title_text.addEventListener('dblclick', () =>
        updateProcess(issue.number),
      );
      description_text.addEventListener('dblclick', () =>
        updateProcess(issue.number),
      );
      const update_button = document.createElement('button');
      const close_button = document.createElement('button');
      update_button.setAttribute('class', 'update-buttons');
      close_button.setAttribute('class', 'update-buttons');
      close_button.setAttribute('id', 'close-button');
      update_button.setAttribute('onclick', `updateProcess(${issue.number})`);
      close_button.setAttribute('onclick', `closeProcess(${issue.number})`);
      update_button.innerText = 'Update Issue';
      close_button.innerText = 'Close Issue';
      div.appendChild(update_button);
      div.appendChild(close_button);
    }
    issues.appendChild(div);
  });
}

// function to get closed issues
async function closedIssues(apiUrl) {
  const closeApiUrl = `${apiUrl}?state=closed`;
  await getIssues(closeApiUrl, 'close');
}

// function to update an issue if edited
async function updateProcess(issueNumber) {
  const current_div = document.getElementById(`issue_id_${issueNumber}`);
  const current_title = current_div.querySelector('input');
  const current_description = current_div.querySelector('textarea');
  current_title.readOnly = false;
  current_description.readOnly = false;

  const buttonsToRemove = current_div.querySelectorAll('button');
  buttonsToRemove.forEach((button) => {
    button.remove();
  });

  const save_changes = document.createElement('button');
  save_changes.setAttribute('id', 'save-button');
  save_changes.setAttribute('class', 'update-buttons');
  save_changes.innerText = 'Save Changes';
  const cancel_changes = document.createElement('button');
  cancel_changes.setAttribute('id', 'cancel-button');
  cancel_changes.setAttribute('class', 'update-buttons');
  cancel_changes.innerText = 'Cancel';

  current_div.appendChild(save_changes);
  current_div.appendChild(cancel_changes);
  save_changes.addEventListener('click', async () => {
    // function to save a change
    const new_title = current_div.querySelector('input');
    const new_description = current_div.querySelector('textarea');

    new_title.readOnly = true;
    new_description.readOnly = true;

    title = new_title.value;
    body = new_description.value;

    const data = { title, body };
    try {
      await CRUDoperation(`${apiUrl}/${issueNumber}`, 'PATCH', data);
      await getIssues(apiUrl); // Wait for the API call to complete
      showPopup('Successfully Updated! Please wait for some time to update.');
    } catch (error) {
      console.error('Error updating description:', error);
    }
  });

  cancel_changes.addEventListener('click', () => {
    // cancel changes midway
    getIssues(apiUrl);
  });
}

// close an issue
async function closeProcess(issueNumber) {
  const state = 'closed';
  await CRUDoperation(`${apiUrl}/${issueNumber}`, 'PATCH', { state });
  await getIssues(apiUrl);
  showPopup('Closed Successfully! Please wait for some time to update.');
}

// reopen a closed issue
async function reopenProcess(issueNumber) {
  const state = 'open';
  await CRUDoperation(`${apiUrl}/${issueNumber}`, 'PATCH', { state });
  await closedIssues(apiUrl);
  showPopup('Reopened Successfully! Please wait for some time to update.');
}

// show the active button with green color
function toggleButton(clickedButton) {
  const buttonList = ['createIssueBtn', 'activeIssuesBtn', 'closedIssuesBtn'];
  buttonList.forEach((button) => {
    if (clickedButton !== button) {
      document.getElementById(button).style.backgroundColor = '#ead7d7';
    } else {
      document.getElementById(button).style.backgroundColor = '#3cb043';
    }
  });
}
// main function
async function main() {
  const activeIssuesBtn = document.getElementById('activeIssuesBtn');
  activeIssuesBtn.addEventListener('click', async () => {
    toggleButton('activeIssuesBtn');
    await getIssues(apiUrl);
  });
  document.addEventListener('DOMContentLoaded', function () {
    // display the active issues when browser reloads
    const activeButton = document.getElementById('activeIssuesBtn');
    activeButton.click();
  });
  const createIssueBtn = document.getElementById('createIssueBtn');
  createIssueBtn.addEventListener('click', () => {
    toggleButton('createIssueBtn');
    const issues = document.querySelector('.content');
    const createIssueContainer = document.querySelector('.create-issue');
    issues.classList.add('display-none');
    createIssueContainer.classList.remove('display-none');
  });

  const closedIssuesBtn = document.getElementById('closedIssuesBtn');
  closedIssuesBtn.addEventListener('click', async () => {
    toggleButton('closedIssuesBtn');
    await closedIssues(apiUrl);
  });
}
main();
